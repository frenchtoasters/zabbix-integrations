from setuptools import setup, find_packages
import ZabbixIntegrations

setup(
    name="ZabbixIntegrations",
    version=ZabbixIntegrations.__version__,
    author=ZabbixIntegrations.__author__,
    description="Create various Zabbix integrations via the API",
    long_description=open('README.md').read(),
    url='https://gitlab.com/frenchtoasters/zabbix-integrations',
    keywords=['tools', 'events', 'pagerduty', 'zabbix', 'integrations', 'interface'],
    license=ZabbixIntegrations.__license__,
    packages=find_packages(exclude=['*.test', '*.test.*']),
    include_package_date=True,
    install_requires=open('requirements.txt').readlines(),
    entry_point={
        'console_scripts': [
            'zabbixpagerduty=ZabbixIntegrations.pagerduty:Pagerduty',
            'zabbixdiscord=ZabbixIntegrations.discord:Discord',
            'zabbixslack=ZabbixIntegrations.slack:Slack'
        ]
    }
)
