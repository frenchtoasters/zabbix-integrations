from ZabbixIntegrations import pagerduty

data = {"pd-service-name": "pd-service-integration-key"}

pd = pagerduty.Pagerduty(
    host="localhost",
    zuser="Admin",
    zpasswd="zabbix",
    data=data,
    pduserpasswd="password"
)

try:
    pd.integrate()
except Exception as e:
    print(e)
