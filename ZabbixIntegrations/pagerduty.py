from pyzabbix import ZabbixAPI


class Pagerduty(object):
    def __init__(self,
                 host: str,
                 zuser: str,
                 zpasswd: str,
                 data: dict,
                 ssl: bool = False,
                 script: str = None,
                 pduserpasswd: str = "PassW0rd!"):
        if ssl:
            self.zapi = ZabbixAPI("https://" + host + "")
            self.frontendurl = "https://" + host + "/"
            self.zapi.login(zuser, zpasswd)
        else:
            self.zapi = ZabbixAPI("http://" + host + "")
            self.frontendurl = "http://" + host + "/"
            self.zapi.login(zuser, zpasswd)
        self.data = data
        self.pduserpasswd = pduserpasswd
        if script:
            self.script = script
        else:
            with open('./ZabbixIntegrations/pagerduty.js', 'r') as file:
                self.script = file.read()

    def integrate(self):
        if not len(self.zapi.usermacro.get(globalmacro=True, filter={"macro": "{$ZABBIX.URL}"})):
            self.zapi.usermacro.createglobal(
                macro="{$ZABBIX.URL}",
                value=self.frontendurl
            )
        for key, value in self.data.items():
            if not len(self.zapi.mediatype.get(filter={"name": key + "-pd-integration"})):
                parameters = [
                    {"name": "token", "value": value},
                    {"name": "url", "value": "{$ZABBIX.URL}"},
                    {"name": "eventname", "value": "{EVENT.NAME}"},
                    {"name": "eventid", "value": "{EVENT.ID}"},
                    {"name": "severity", "value": "{EVENT.SEVERITY}"},
                    {"name": "triggeropdata", "value": "{EVENT.OPDATA}"},
                    {"name": "eventtags", "value": "{EVENT.TAGS}"},
                    {"name": "eventtime", "value": "{EVENT.TIME}"},
                    {"name": "eventdate", "value": "{EVENT.DATE}"},
                    {"name": "eventack", "value": "{EVENT.ACK.STATUS}"},
                    {"name": "eventupdate", "value": "{EVENT.UPDATE.STATUS}"},
                    {"name": "eventvalue", "value": "{EVENT.VALUE}"},
                    {"name": "hostname", "value": "{HOST.NAME}"},
                    {"name": "triggerdescription", "value": "{TRIGGER.DESCRIPTION}"},
                    {"name": "triggerid", "value": "{TRIGGER.ID}"}
                ]
                self.zapi.mediatype.create(
                    name=key + "-pd-integration",
                    type=4,
                    parameters=parameters,
                    show_event_menu=0,
                    script=self.script
                )
            if not len(self.zapi.usergroup.get(filter={"name": key+"-group"})):
                self.zapi.usergroup.create(
                    name=key+"-group"
                )
            if not len(self.zapi.user.get(filter={"alias": "pagerduty-" + key})):
                media = self.zapi.mediatype.get(filter={"name": key + "-pd-integration"})
                usrgrp = self.zapi.usergroup.get(filter={"name": key+"-group"})
                medias = [{
                    "mediatypeid": media[0]['mediatypeid'],
                    "sendto": [value]
                }]
                usrgrps = [{
                    "usrgrpid": usrgrp[0]['usrgrpid']
                }]
                self.zapi.user.create(
                    alias="pagerduty-"+key,
                    passwd=self.pduserpasswd,
                    usrgrps=usrgrps,
                    user_medias=medias
                )
            if not len(self.zapi.action.get(filter={"name": key+"-action"})):
                user_id = self.zapi.user.get(filter={"alias": "pagerduty-" + key})
                media_id = self.zapi.mediatype.get(filter={"name": key + "-pd-integration"})
                # This filter is opinion based that, in that you should tag all hosts/triggers in zabbix with the
                # corresponding pagerduty service. If you do not agree with this you can simply remove conditions[0].
                # Additionally it also assumes that you would only like to have pagerduty notified of alerts that are of
                # severity Average or above. See the documentation below if you would like to change it.
                # https://www.zabbix.com/documentation/4.0/manual/api/reference/action/object#action_filter
                action_filter = {
                    "evaltype": 1,
                    "conditions": [
                        {
                            "conditiontype": 26,
                            "operator": 0,
                            "value": key,
                            "value2": "pd-service"
                        },
                        {
                            "conditiontype": 4,
                            "operator": 5,
                            "value": 3
                        }
                    ]
                }
                trigger_operation = [{
                    "operationtype": 0,
                    "opmessage_usr": [{"userid": user_id[0]['userid']}],
                    "opmessage": {
                        "message": "name:{TRIGGER.NAME}\r\nid:{TRIGGER.ID}\r\nstatus:{TRIGGER.STATUS}\r\n"
                                   "hostname:{HOSTNAME}\r\nip:{IPADDRESS}\r\nvalue:{TRIGGER.VALUE}\r\n"
                                   "event_id:{EVENT.ID}\r\nseverity:{TRIGGER.SEVERITY}",
                        "subject": "trigger",
                        "mediatypeid": media_id[0]['mediatypeid']
                    }
                }]
                resolve_operation = [{
                    "operationtype": 0,
                    "opmessage_usr": [{"userid": user_id[0]['userid']}],
                    "opmessage": {
                        "message": "name:{TRIGGER.NAME}\r\nid:{TRIGGER.ID}\r\nstatus:{TRIGGER.STATUS}\r\n"
                                   "hostname:{HOSTNAME}\r\nip:{IPADDRESS}\r\nvalue:{TRIGGER.VALUE}\r\n"
                                   "event_id:{EVENT.ID}\r\nseverity:{TRIGGER.SEVERITY}",
                        "subject": "resolve",
                        "mediatypeid": media_id[0]['mediatypeid']
                    }
                }]

                self.zapi.action.create(
                    name=key+"-action",
                    esc_period="1h",
                    eventsource=0,
                    status=0,
                    filter=action_filter,
                    operations=trigger_operation,
                    recovery_operations=resolve_operation
                )