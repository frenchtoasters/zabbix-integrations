from pyzabbix import ZabbixAPI


class Discord(object):
    def __init__(self,
                 host: str,
                 zuser: str,
                 zpasswd: str,
                 data: dict,
                 ssl: bool = False,
                 script: str = None,
                 discuserpasswd: str = "PassW0rd!"):
        if ssl:
            self.zapi = ZabbixAPI("https://" + host)
            self.frontendurl = "https://" + host + "/"
            self.zapi.login(zuser, zpasswd)
        else:
            self.zapi = ZabbixAPI("http://" + host)
            self.frontendurl = "http://" + host + "/"
            self.zapi.login(zuser, zpasswd)
        self.data = data
        self.discuserpasswd = discuserpasswd
        if script:
            self.script = script
        else:
            with open('./ZabbixIntegrations/discord.js', 'r') as file:
                self.script = file.read()

    def integrate(self):
        if not len(self.zapi.usermacro.get(globalmacro=True, filter={"macro": "{$ZABBIX.URL}"})):
            self.zapi.usermacro.createglobal(
                macro="{$ZABBIX.URL}",
                value=self.frontendurl
            )
        if not len(self.zapi.mediatype.get(filter={"name": "discord-integration"})):
            parameters = [
                {"name": "zabbix_url", "value": "{$ZABBIX.URL}"},
                {"name": "discord_endpoint", "value": "{ALERT.SENDTO}"},
                {"name": "use_default_message", "value": "false"},
                {"name": "alert_message", "value": "{ALERT.MESSAGE}"},
                {"name": "alert_subject", "value": "{ALERT.SUBJECT}"},
                {"name": "event_name", "value": "{EVENT.NAME}"},
                {"name": "event_id", "value": "{EVENT.ID}"},
                {"name": "event_severity", "value": "{EVENT.SEVERITY}"},
                {"name": "event_nseverity", "value": "{EVENT.NSEVERITY}"},
                {"name": "event_opdata", "value": "{EVENT.OPDATA}"},
                {"name": "event_tags", "value": "{EVENT.TAGS}"},
                {"name": "event_time", "value": "{EVENT.TIME}"},
                {"name": "event_date", "value": "{EVENT.DATE}"},
                {"name": "event_recovery_time", "value": "{EVENT.RECOVERY.TIME}"},
                {"name": "event_update_time", "value": "{EVENT.UPDATE.TIME}"},
                {"name": "event_update_message", "value": "{EVENT.UPDATE.MESSAGE}"},
                {"name": "event_update_status", "value": "{EVENT.UPDATE.STATUS}"},
                {"name": "event_update_user", "value": "{USER.FULLNAME}"},
                {"name": "event_value", "value": "{EVENT.VALUE}"},
                {"name": "host_ip", "value": "{HOST.IP}"},
                {"name": "host_name", "value": "{HOST.NAME}"},
                {"name": "trigger_description", "value": "{TRIGGER.DESCRIPTION}"},
                {"name": "trigger_id", "value": "{TRIGGER.ID}"}
            ]
            self.zapi.mediatype.create(
                name="discord-integration",
                type=4,
                parameters=parameters,
                maxattempts=5,
                attempt_interval="11s",
                script=self.script
            )
        for key, value in self.data.items():
            if not len(self.zapi.usergroup.get(filter={"name": key+"-group"})):
                self.zapi.usergroup.create(
                    name=key+"-group"
                )
            if not len(self.zapi.user.get(filter={"alias": "discord-" + key})):
                media = self.zapi.mediatype.get(filter={"name": "discord-integration"})
                usrgrp = self.zapi.usergroup.get(filter={"name": key + "-group"})
                medias = [{
                    "mediatypeid": media[0]['mediatypeid'],
                    "sendto": [value]
                }]
                usrgrps = [{
                    "usrgrpid": usrgrp[0]['usrgrpid']
                }]
                self.zapi.user.create(
                    alias="discord-" + key,
                    passwd=self.discuserpasswd,
                    usrgrps=usrgrps,
                    user_medias=medias
                )
            if not len(self.zapi.action.get(filter={"name": key + "-action"})):
                # This filter is opinion based, in that it also assumes that you would only like to have discord
                # notified of alerts that are of severity Average or above. See the documentation below if you would
                # like to change it.
                # https://www.zabbix.com/documentation/4.0/manual/api/reference/action/object#action_filter
                user_id = self.zapi.user.get(filter={"alias": "discord-" + key})
                media_id = self.zapi.mediatype.get(filter={"name": "discord-integration"})

                action_filter = {
                    "evaltype": 1,
                    "conditions": [
                        {
                            "conditiontype": 4,
                            "operator": 5,
                            "value": 3
                        }
                    ]
                }
                trigger_operation = [{
                    "operationtype": 0,
                    "opmessage_usr": [{"userid": user_id[0]['userid']}],
                    "opmessage": {
                        "message": "name:{TRIGGER.NAME}\r\nid:{TRIGGER.ID}\r\nstatus:{TRIGGER.STATUS}\r\n"
                                   "hostname:{HOSTNAME}\r\nip:{IPADDRESS}\r\nvalue:{TRIGGER.VALUE}\r\n"
                                   "event_id:{EVENT.ID}\r\nseverity:{TRIGGER.SEVERITY}",
                        "subject": "trigger",
                        "mediatypeid": media_id[0]['mediatypeid']
                    }
                }]
                resolve_operation = [{
                    "operationtype": 0,
                    "opmessage_usr": [{"userid": user_id[0]['userid']}],
                    "opmessage": {
                        "message": "name:{TRIGGER.NAME}\r\nid:{TRIGGER.ID}\r\nstatus:{TRIGGER.STATUS}\r\n"
                                   "hostname:{HOSTNAME}\r\nip:{IPADDRESS}\r\nvalue:{TRIGGER.VALUE}\r\n"
                                   "event_id:{EVENT.ID}\r\nseverity:{TRIGGER.SEVERITY}",
                        "subject": "resolve",
                        "mediatypeid": media_id[0]['mediatypeid']
                    }
                }]

                self.zapi.action.create(
                    name=key + "-action",
                    esc_period="1h",
                    eventsource=0,
                    status=0,
                    filter=action_filter,
                    operations=trigger_operation,
                    recovery_operations=resolve_operation
                )