try {

    var params = JSON.parse(value),
        req = new CurlHttpRequest(),
        fields = {},
        resp = '';

    if (isNaN(parseInt(params.eventid)) || params.eventid < 1) {
        throw 'incorrect value for variable "eventid". The value must be a positive number.';
    }
    if (params.eventname.length < 1) {
        throw 'incorrect value for variable "eventname". The value must be a non-empty string.';
    }
    if (params.hostname.length < 1) {
        throw 'incorrect value for variable "eventname". The value must be a non-empty string.';
    }
    if (isNaN(parseInt(params.severity)) || (params.severity < 0 && params.severity > 5)) {
        throw 'incorrect value for variable "severity". The value must be a number 0..5.';
    }
    if (isNaN(parseInt(params.triggerid)) || params.triggerid < 1) {
        throw 'incorrect value for variable "triggerid". The value must be a positive number.';
    }
    if (params.eventack != 'Yes' && params.eventack != 'No') {
        throw 'incorrect value for variable "eventack". The value must be Yes or No.';
    }
    if (isNaN(parseInt(params.eventupdate)) || (params.eventupdate < 0 || params.eventupdate > 1)) {
        throw 'incorrect value for variable "eventupdate". The value must be 0 or 1.';
    }
    if (isNaN(parseInt(params.eventvalue)) || (params.eventvalue < 0 || params.eventvalue > 1)) {
        throw 'incorrect value for variable "eventvalue". The value must be 0 or 1.';
    }

    // Correspondence between the PagerDuty and Zabbix severity level
    var severityMapping = [
        'info',    // Not classified
        'info',    // Information
        'warning', // Warning
        'warning', // Average
        'error',   // High
        'critical' // Disaster
    ];

    req.AddHeader('Content-Type: application/json');

    fields.routing_key = params.token;
    fields.dedup_key = params.eventid;

    if ((params.eventvalue == 1) && (params.eventupdate == 0)) {
        fields.event_action = 'trigger';
        fields.payload = {
            summary: params.eventname,
            source: params.hostname + ' : ' + params.hostip,
            severity: severityMapping[params.severity],
            custom_details: {
                'Event date': params.eventdate,
                'Event time': params.eventtime,
                'Trigger description': params.triggerdesc,
                'Trigger opdata': params.triggeropdata,
                'Event tags': params.eventtags,
                'Event host': params.hostname,
                'Event host ip': params.hostip
            }
        };
        fields.links = [{
            href: params.url + '/tr_events.php?triggerid=' + params.triggerid + '&eventid=' + params.eventid,
            text: 'Event link'
        }];
        fields.client = 'Zabbix';
        fields.client_url = params.url;
    }
    else if ((params.eventvalue == 1) && (params.eventupdate == 1) && (params.eventack == 'Yes'))
        fields.event_action = 'acknowledge';
    else if (params.eventvalue == 0)
        fields.event_action = 'resolve';
    else
        throw 'incorrect values. Update message without ack will not be sent.';

    Zabbix.Log(4, '[PagerDuty Webhook] Sending request:' + JSON.stringify(fields));
    resp = req.Post('https://events.pagerduty.com/v2/enqueue',
        JSON.stringify(fields)
    );
    Zabbix.Log(4, '[PagerDuty Webhook] Receiving response:' + resp);

    try {
        resp = JSON.parse(resp);
    }
    catch (error) {
        throw 'incorrect response. PagerDuty returned a non-JSON object.';
    }

    if (req.Status() != 202) {
        if (typeof resp === 'object' && typeof resp.errors === 'object' && typeof resp.errors[0] === 'string') {
            throw resp.errors[0];
        }
        else {
            throw 'Unknown error.';
        }
    }

    if (resp.status != 'success') {
        throw 'Unknown error.';
    }

    return 'OK';
}
catch (error) {
    Zabbix.Log(3, '[PagerDuty Webhook] Notification failed : ' + error);
    throw 'PagerDuty notification failed : ' + error;
}