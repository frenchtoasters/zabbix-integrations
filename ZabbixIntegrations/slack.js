var SEVERITY_COLORS = [
	'#97AAB3', '#7499FF', '#FFC859',
	'#FFA059', '#E97659', '#E45959'
];

var RESOLVE_COLOR = '#009900';

var SLACK_MODE_HANDLERS = {
	alarm: handlerAlarm,
	event: handlerEvent
};

var EVENT_STATUS = {
	PROBLEM: 'PROBLEM',
	UPDATE: 'UPDATE',
	RESOLVE: 'OK'
}

if (!String.prototype.format) {
 	String.prototype.format = function() {
   		var args = arguments;
   		return this.replace(/{(\d+)}/g, function(match, number) {
			return number in args
				? args[number]
				: match
			;
		});
	};
}

function isEventProblem(params) {
	return params.event_value == 1
		&& params.event_update_status == 0
		;
}

function isEventUpdate(params) {
	return params.event_value == 1
		&& params.event_update_status == 1
		// && params.event_ack_status == 'Yes'
		;
}

function isEventResolve(params) {
	return params.event_value == 0;
}

function getPermalink(channelId, messageTimestamp) {
	var req = new CurlHttpRequest();
	req.AddHeader('Content-Type: application/x-www-form-urlencoded; charset=utf-8');

	var resp = JSON.parse(req.Get(
		'{0}?token={1}&channel={2}&message_ts={3}'.format(
			Slack.getPermalink,
			params.bot_token,
			channelId,
			messageTimestamp
			)
		));

	if (req.Status != 200 && !resp.ok) {
		throw resp.error;
	}

	return resp.permalink;
}

function createProblemURL(zabbixURL, triggerId, eventId) {
	var problemURL = '{0}/tr_events.php?triggerid={1}&eventid={2}'
		.format(
			zabbixURL.replace(/\/+$/, ''),
			triggerId,
			eventId
		);

	return problemURL
}

function getTagValue(eventTags, key) {
	var pattern = new RegExp('(' + key + ':.+)');
	var tagValue = eventTags
		.split(',')
		.filter(function (v) {return v.match(pattern)})
		.map(function (v) {return v.split(':')[1]})[0]
		|| 0;

	return tagValue;
}

function handlerAlarm(params) {
	var fields = {
		channel: params.channel,
		as_user: params.slack_as_user,
	}

	if (isEventProblem(params)) {
		fields.attachments = [
			createMessage(
				EVENT_STATUS.PROBLEM,
				SEVERITY_COLORS[params.event_nseverity] || 0,
				params.event_date,
				params.event_time,
				createProblemURL(params.zabbix_url, params.trigger_id, params.event_id)
				)
			];

		var resp = JSON.parse(req.Post(Slack.postMessage, JSON.stringify(fields)));
		if (req.Status != 200 && !resp.ok) {
			throw resp.error;
		}

		result.tags.__message_ts = resp.ts;
		result.tags.__channel_id = resp.channel;
		result.tags.__channel_name = params.channel;
		result.tags.__message_link = getPermalink(resp.channel, resp.ts);

	} else if (isEventUpdate(params)) {
		fields.thread_ts = getTagValue(params.event_tags, 'message_ts');
		fields.attachments = [
			createMessage(
				EVENT_STATUS.UPDATE,
				SEVERITY_COLORS[params.event_nseverity] || 0,
				params.event_update_date,
				params.event_update_time,
				createProblemURL(params.zabbix_url, params.trigger_id, params.event_id),
				true,
				params.event_update_message
				)
			];

		var resp = JSON.parse(req.Post(Slack.postMessage, JSON.stringify(fields)));
		if (req.Status != 200 && !resp.ok) {
			throw resp.error;
		}

	} else if (isEventResolve(params)) {
		fields.channel = getTagValue(params.event_tags, 'channel_id');
		fields.text = '';
		fields.ts = getTagValue(params.event_tags, 'message_ts');
		fields.attachments = [
			createMessage(
				EVENT_STATUS.RESOLVE,
				RESOLVE_COLOR,
				params.event_date,
				params.event_time,
				createProblemURL(params.zabbix_url, params.trigger_id, params.event_id)
				)
			];

		var resp = JSON.parse(req.Post(Slack.chatUpdate, JSON.stringify(fields)));
		if (req.Status != 200 && !resp.ok) {
			throw resp.error;
		}
	}
}

function handlerEvent(params) {
	var fields = {
		channel: params.channel,
		as_user: params.slack_as_user
	}

	if (isEventProblem(params)) {
		fields.attachments = [
			createMessage(
				EVENT_STATUS.PROBLEM,
				SEVERITY_COLORS[params.event_nseverity] || 0,
				params.event_date,
				params.event_time,
				createProblemURL(params.zabbix_url, params.trigger_id, params.event_id)
				)
			];

		var resp = JSON.parse(req.Post(Slack.postMessage, JSON.stringify(fields)));
		if (req.Status != 200 && !resp.ok) {
			throw resp.error;
		}

		result.tags.__channel_name = params.channel;
		result.tags.__message_link = getPermalink(resp.channel, resp.ts);

	} else if (isEventUpdate(params)) {
		fields.attachments = [
			createMessage(
				EVENT_STATUS.UPDATE,
				SEVERITY_COLORS[params.event_nseverity] || 0,
				params.event_update_date,
				params.event_update_time,
				createProblemURL(params.zabbix_url, params.trigger_id, params.event_id),
				false,
				params.event_update_message
				)
			];

		var resp = JSON.parse(req.Post(Slack.postMessage, JSON.stringify(fields)));
		if (req.Status != 200 && !resp.ok) {
			throw resp.error;
		}

	} else if (isEventResolve(params)) {
		fields.attachments = [
			createMessage(
				EVENT_STATUS.RESOLVE,
				RESOLVE_COLOR,
				params.event_recovery_date,
				params.event_recovery_time,
				createProblemURL(params.zabbix_url, params.trigger_id, params.event_id)
				)
			];

		var resp = JSON.parse(req.Post(Slack.postMessage, JSON.stringify(fields)));
		if (req.Status != 200 && !resp.ok) {
			throw resp.error;
		}
	}
}

function createMessage(
	status,
	eventSeverityColor,
	eventDate,
	eventTime,
	problemURL,
	isShort,
	messageText
	) {
	var message = {
		'fallback': '{0}: {1}'.format(status, params.event_name),
		'title': '{0}: {1}'.format(status, params.event_name),
		'color': eventSeverityColor,
		'title_link': problemURL,
		'pretext': messageText || '',

		'fields': [
			{
				'title': 'Host',
				'value': '{0} [{1}]'.format(params.host_name, params.host_ip),
				'short': true
			},
			{
				'title': 'Event time',
				'value': '{0} {1}'.format(eventDate, eventTime),
				'short': true
			},
			{
				'title': 'Severity',
				'value': params.event_severity,
				'short': true
			},
			{
				'title': 'Opdata',
				'value': params.event_opdata,
				'short': true
			}
		],
	};

	if (!isShort) {
		message['actions'] = [
			{
                "type": "button",
                "text": "Open in Zabbix",
                "url": problemURL
            }
		];

		message.fields.push(
			{
				'title': 'Event tags',
				'value': params.event_tags.replace(/__.+?:(.+?,|.+)/g, '') || 'None',
				'short': true
			},
			{
				'title': 'Trigger description',
				'value': params.trigger_description,
				'short': true
			}
			)
	}

	return message;
}

try {
	var params = JSON.parse(value),
		req = new CurlHttpRequest(),
		fields = {},
		result = {tags: {}};


	req.AddHeader('Content-Type: application/json; charset=utf-8');
	req.AddHeader('Authorization: Bearer ' + params.bot_token)

	var Slack = {
		postMessage: params.slack_endpoint + 'chat.postMessage',
		getPermalink: params.slack_endpoint + 'chat.getPermalink',
		chatUpdate: params.slack_endpoint + 'chat.update'
	}

	params.slack_mode = params.slack_mode.toLowerCase();
	params.slack_mode = params.slack_mode in SLACK_MODE_HANDLERS
		? params.slack_mode
		: 'alarm';

	SLACK_MODE_HANDLERS[params.slack_mode](params);
	return JSON.stringify(result);

} catch (error) {
	Zabbix.Log(3, 'Slack notification failed : ' + error);
	throw 'Slack notification failed : ' + error;
}