from pyzabbix import ZabbixAPI
import logging, sys

class Slack(object):
    def __init__(self,
                 host: str,
                 zuser: str,
                 zpasswd: str,
                 slacktoken: str,
                 data: dict,
                 ssl: bool = False,
                 script: str = None,
                 slackuserpasswd: str = "PassW0rd!"):
        if ssl:
            self.zapi = ZabbixAPI("https://" + host)
            self.frontendurl = "https://" + host + "/"
            self.zapi.login(zuser, zpasswd)
        else:
            self.zapi = ZabbixAPI("http://" + host)
            self.frontendurl = "http://" + host + "/"
            self.zapi.login(zuser, zpasswd)
        self.data = data
        self.slackuserpasswd = slackuserpasswd
        self.frontendurl = host
        self.slacktoken = slacktoken
        if script:
            self.script = script
        else:
            with open('./ZabbixIntegrations/slack.js', 'r') as file:
                self.script = file.read()

        stream = logging.StreamHandler(sys.stdout)
        stream.setLevel(logging.DEBUG)
        log = logging.getLogger('pyzabbix')
        log.addHandler(stream)
        log.setLevel(logging.DEBUG)

    def integrate(self):
        if not len(self.zapi.usermacro.get(globalmacro=True, filter={"macro": "{$ZABBIX.URL}"})):
            self.zapi.usermacro.createglobal(
                macro="{$ZABBIX.URL}",
                value=self.frontendurl
            )
        if not len(self.zapi.mediatype.get(filter={"name": "slack-integration"})):
            parameters = [
                {"name": "zabbix_url", "value": "{$ZABBIX.URL}"},
                {"name": "bot_token", "value": self.slacktoken},
                {"name": "channel", "value": "{ALERT.SENDTO}"},
                {"name": "slack_mode", "value": "alarm"},
                {"name": "slack_as_user", "value": "true"},
                {"name": "slack_endpoint", "value": "https://slack.com/api/"},
                {"name": "event_id", "value": "{EVENT.ID}"},
                {"name": "event_name", "value": "{EVENT.NAME}"},
                {"name": "event_severity", "value": "{EVENT.SEVERITY}"},
                {"name": "event_nseverity", "value": "{EVENT.NSEVERITY}"},
                {"name": "event_opdata", "value": "{EVENT.OPDATA}"},
                {"name": "event_tags", "value": "{EVENT.TAGS}"},
                {"name": "event_time", "value": "{EVENT.TIME}"},
                {"name": "event_date", "value": "{EVENT.DATE}"},
                {"name": "event_recovery_time", "value": "{EVENT.RECOVERY.TIME}"},
                {"name": "event_update_date", "value": "{EVENT.UPDATE.DATE}"},
                {"name": "event_update_time", "value": "{EVENT.UPDATE.TIME}"},
                {"name": "event_update_message", "value": "{EVENT.UPDATE.MESSAGE}"},
                {"name": "event_ack_status", "value": "{EVENT.ACK.STATUS}"},
                {"name": "event_update_status", "value": "{EVENT.UPDATE.STATUS}"},
                {"name": "event_recovery_date", "value": "{EVENT.RECOVERY.DATE}"},
                {"name": "event_value", "value": "{EVENT.VALUE}"},
                {"name": "host_ip", "value": "{HOST.IP}"},
                {"name": "host_name", "value": "{HOST.NAME}"},
                {"name": "trigger_description", "value": "{TRIGGER.DESCRIPTION}"},
                {"name": "trigger_id", "value": "{TRIGGER.ID}"}
            ]
            self.zapi.mediatype.create(
                name="slack-integration",
                type=4,
                parameters=parameters,
                script=self.script,
                process_tags=1,
                show_event_menu=1,
                event_menu_url="{EVENT.TAGS.__message_link}",
                event_menu_name="Open in Slack: {EVENT.TAGS.__channel_name}"
            )
        for key, value in self.data.items():
            if not len(self.zapi.usergroup.get(filter={"name": key + "-group"})):
                self.zapi.usergroup.create(
                    name=key+"-group"
                )
            if not len(self.zapi.user.get(filter={"alias": "slack-" + key})):
                media = self.zapi.mediatype.get(filter={"name": "slack-integration"})
                usrgrp = self.zapi.usergroup.get(filter={"name": key + "-group"})
                medias = [{
                    "mediatypeid": media[0]['mediatypeid'],
                    "sendto": [value]
                }]
                usrgrps = [{
                    "usrgrpid": usrgrp[0]['usrgrpid']
                }]
                self.zapi.user.create(
                    alias="slack-" + key,
                    passwd=self.slackuserpasswd,
                    usrgrps=usrgrps,
                    user_medias=medias
                )
            if not len(self.zapi.action.get(filter={"name": key + "-action"})):
                user_id = self.zapi.user.get(filter={"alias": "slack-" + key})
                media_id = self.zapi.mediatype.get(filter={"name": "slack-integration"})
                # This filter is opinion based, in that it also assumes that you would only like to have slack
                # notified of alerts that are of severity Average or above. See the documentation below if you would
                # like to change it.
                # https://www.zabbix.com/documentation/4.0/manual/api/reference/action/object#action_filter
                action_filter = {
                    "evaltype": 1,
                    "conditions": [
                        {
                            "conditiontype": 4,
                            "operator": 5,
                            "value": 3
                        }
                    ]
                }
                trigger_operation = [{
                    "operationtype": 0,
                    "opmessage_usr": [{"userid": user_id[0]['userid']}],
                    "opmessage": {
                        "message": "name:{TRIGGER.NAME}\r\nid:{TRIGGER.ID}\r\nstatus:{TRIGGER.STATUS}\r\n"
                                   "hostname:{HOSTNAME}\r\nip:{IPADDRESS}\r\nvalue:{TRIGGER.VALUE}\r\n"
                                   "event_id:{EVENT.ID}\r\nseverity:{TRIGGER.SEVERITY}",
                        "subject": "trigger",
                        "mediatypeid": media_id[0]['mediatypeid']
                    }
                }]
                resolve_operation = [{
                    "operationtype": 0,
                    "opmessage_usr": [{"userid": user_id[0]['userid']}],
                    "opmessage": {
                        "message": "name:{TRIGGER.NAME}\r\nid:{TRIGGER.ID}\r\nstatus:{TRIGGER.STATUS}\r\n"
                                   "hostname:{HOSTNAME}\r\nip:{IPADDRESS}\r\nvalue:{TRIGGER.VALUE}\r\n"
                                   "event_id:{EVENT.ID}\r\nseverity:{TRIGGER.SEVERITY}",
                        "subject": "resolve",
                        "mediatypeid": media_id[0]['mediatypeid']
                    }
                }]

                self.zapi.action.create(
                    name=key+"-action",
                    esc_period="1h",
                    eventsource=0,
                    status=0,
                    filter=action_filter,
                    operations=trigger_operation,
                    recovery_operations=resolve_operation
                )