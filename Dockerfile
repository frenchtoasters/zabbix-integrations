FROM python:3.8-slim

WORKDIR /opt/integrations

COPY requirements.txt /opt/integrations

RUN pip install -r requirements.txt

ADD ZabbixIntegrations /opt/integrations/ZabbixIntegrations
COPY setup.cfg /opt/integrations
COPY setup.py /opt/integrations

RUN python setup.py install