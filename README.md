# Zabbix Integrations

This repo contains a command line tool and python library for installing and configuring various Zabbix integrations.

## Integrations

* Pagerduty (usergroup, user, mediatype, action)
* Slack (usergroup, user, mediatype, action)
* Discord (usergroup, user, mediatype, action)
* Mattermost**
* Microsoft Teams**

** == not implemented
