from ZabbixIntegrations import slack

data = {"channel": "token"}

slack = slack.Slack(
    host="localhost",
    zuser="Admin",
    zpasswd="zabbix",
    slacktoken="tokenxxxx",
    data=data,
    slackuserpasswd="password"
)

try:
    slack.integrate()
except Exception as e:
    print(e)
